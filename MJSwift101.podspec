Pod::Spec.new do |s|
  s.name             = 'MJSwift101'
  s.version          = '0.1.0'
  s.summary          = 'A short description of MJSwift101.'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/davidthorn/swift101.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'David Thorn' => 'david.thorn@atino.de' }
  s.source           = { :git => 'https://bitbucket.org/davidthorn/swift101.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'MJSwift101/Classes/**/*'
  
s.resource_bundles = {
  'MJSwift101' => ['MJSwift101/Assets/*']
}
  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
