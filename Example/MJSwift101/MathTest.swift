//
//  MathTest.swift
//  MJSwift101
//
//  Created by David Thorn on 07/09/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation

protocol MathTestProtocol
{
    func doMath( num1 : Int , num2: Int ) -> Int
}


class AddMathTest: MathTestProtocol{
    
    
    init(){
        
    }
    
    func doMath( num1: Int , num2: Int ) -> Int{
        return num1 + num2
    }
    
    
    
}


class MinusMathTest: MathTestProtocol{
    
    
    init(){
        
    }
    
    func doMath( num1: Int , num2: Int ) -> Int{
        return num1 - num2
    }
    
    
    
}

class DivMathTest: MathTestProtocol{
    
    init(){
        
    }
    
    
    func doMath( num1: Int , num2: Int ) -> Int{
        return num1 * num2
    }
    
    
    
}