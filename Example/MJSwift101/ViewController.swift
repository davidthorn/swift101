//
//  ViewController.swift
//  MJSwift101
//
//  Created by David Thorn on 09/07/2016.
//  Copyright (c) 2016 David Thorn. All rights reserved.
//

import UIKit

enum MathSign{
    case ADD
    case MINUS
    case MULT
    case DIV
}

class ViewController: UIViewController {

    @IBOutlet weak var rightNumText: UITextField!
    @IBOutlet weak var leftNumText: UITextField!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    func getLeftNumText() -> String?{
        
        if let num = self.leftNumText.text{
            return num
        }
        
        //
        
        
        return nil
        
    }
    
    func getRightNumText() -> String?{
        if let num = self.rightNumText.text{
            return num
        }
        
        return nil
    }
    
    
    @IBAction func minusButton(sender: AnyObject) {
        
        let numbers = self.getNumbers()
        
        let number = self.doMath(.MINUS, num1: numbers.num1, num2: numbers.num2)
        
        print( number )
        
    }
    
    
    
    
    @IBAction func multButton(sender: AnyObject) {
        let numbers = self.getNumbers()
        
        let number = self.doMath(.MULT, num1: numbers.num1, num2: numbers.num2)
        print( number )
    }
    
    @IBAction func divButton(sender: AnyObject) {
        let numbers = self.getNumbers()
        
        let number = self.doMath(.DIV, num1: numbers.num1, num2: numbers.num2)
        print( number )
    }
    @IBAction func plusButton(sender: AnyObject) {
        let numbers = self.getNumbers()
        
        let number = AddMathTest().doMath(numbers.num1, num2: numbers.num2)
        
        //let number = self.doMath(.ADD, num1: numbers.num1, num2: numbers.num2)
        print( number )
    }

    
    var errorHappened: Bool = false
    
    func getNumbers() -> ( num1: Int , num2: Int ){
       
        var realNum1 = 0
        var realNum2 = 0
        
        if let num1 = self.getLeftNumText(){
            if let x = Int(num1){
                realNum1 = x
            }
        }
        
        if let num2 = self.getRightNumText(){
            if let x = Int(num2){
                realNum2 = x
            }
        }
        
        

        return ( realNum1 , realNum2 )
    }
    
    func doMath( sign: MathSign , num1: Int , num2: Int ) -> Int{
        switch sign{
        case .ADD:
            return AddMathTest().doMath(num1, num2: num2)
        case .MINUS:
            return num1 - num2
        case .DIV:
            if num1 != 0 && num2 != 0{
                return num1 / num2
            }
            return 0
        case .MULT:
            return num1 * num2
        }
        
        return 0
    }
    
    
}

