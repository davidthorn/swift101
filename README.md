# MJSwift101

[![CI Status](http://img.shields.io/travis/David Thorn/MJSwift101.svg?style=flat)](https://travis-ci.org/David Thorn/MJSwift101)
[![Version](https://img.shields.io/cocoapods/v/MJSwift101.svg?style=flat)](http://cocoapods.org/pods/MJSwift101)
[![License](https://img.shields.io/cocoapods/l/MJSwift101.svg?style=flat)](http://cocoapods.org/pods/MJSwift101)
[![Platform](https://img.shields.io/cocoapods/p/MJSwift101.svg?style=flat)](http://cocoapods.org/pods/MJSwift101)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MJSwift101 is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MJSwift101"
```

## Author

David Thorn, david.thorn@atino.de

## License

MJSwift101 is available under the MIT license. See the LICENSE file for more info.
